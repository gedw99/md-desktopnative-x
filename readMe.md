# md-desktopnative-x

https://bitbucket.org/gedw99/md-desktopnative-x

This shows how to build native desktop applications. It is designed to act as a best practice example that covers corner cases.

It runs on:
- Windows
- OSX
- Linux

## Setup
- Golang 1.7
- Gcc
    - On OSX, you just need xcode
    - On Windows download from https://sourceforge.net/projects/mingw-w64/files/Toolchains%20targetting%20Win32/Personal%20Builds/mingw-builds/installer/
    - on Linux, ???

## Imports
- github.com/andlabs/ui
    - fork: github.com/ProtonMail/ui  
        - docs: https://godoc.org/github.com/ProtonMail/ui


## Roadmap
- [ ] Examples 
    - [ ] Component Kitchen, showing each widget
    - [x] Hello world
    - [ ] Composition via reusable components. 
    - [ ] Localisation
    - [ ] Modal dialog, that blocks parent window until closed
- [ ] Packaging
    - [ ] app icon
    - [ ] auto-start on OS startup
    - [ ] OS Packaging
    - [ ] auto update via S3 (or other)
- [ ] Embed Electron.

- [ ] BoltDB db
    - [ ] data
    - [ ] indexing





