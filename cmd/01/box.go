// 12 december 2015

package main

import (
	"github.com/andlabs/ui"
)

var page1 *ui.Box

func makePage1(w *ui.Window) {

	var xbutton *ui.Button

	// page1
	page1 = ui.NewVerticalBox()

	entry := ui.NewEntry()
	page1.Append(entry, false)

	spaced := ui.NewCheckbox("Spaced")
	spaced.OnToggled(func(*ui.Checkbox) {
		setSpaced(spaced.Checked())
	})

	label := ui.NewLabel("Label")

	// hbox
	hbox := newHorizontalBox()
	getButton := ui.NewButton("Get Window Text")
	getButton.OnClicked(func(*ui.Button) {
		entry.SetText(w.Title())
	})
	setButton := ui.NewButton("Set Window Text")
	setButton.OnClicked(func(*ui.Button) {
		w.SetTitle(entry.Text())
	})
	hbox.Append(getButton, true)
	hbox.Append(setButton, true)
	page1.Append(hbox, false)

	hbox = newHorizontalBox()
	getButton = ui.NewButton("Get Button Text")
	xbutton = getButton
	getButton.OnClicked(func(*ui.Button) {
		entry.SetText(xbutton.Text())
	})
	setButton = ui.NewButton("Set Button Text")
	setButton.OnClicked(func(*ui.Button) {
		xbutton.SetText(entry.Text())
	})

	hbox.Append(getButton, true)
	hbox.Append(setButton, true)
	page1.Append(hbox, false)

	hbox = newHorizontalBox()
	getButton = ui.NewButton("Get Checkbox Text")
	getButton.OnClicked(func(*ui.Button) {
		entry.SetText(spaced.Text())
	})
	setButton = ui.NewButton("Set Checkbox Text")
	setButton.OnClicked(func(*ui.Button) {
		spaced.SetText(entry.Text())
	})

	// compose
	hbox.Append(getButton, true)
	hbox.Append(setButton, true)
	page1.Append(hbox, false)

	// hbox
	hbox = newHorizontalBox()
	getButton = ui.NewButton("Get Label Text")
	getButton.OnClicked(func(*ui.Button) {
		entry.SetText(label.Text())
	})
	setButton = ui.NewButton("Set Label Text")
	setButton.OnClicked(func(*ui.Button) {
		label.SetText(entry.Text())
	})
	hbox.Append(getButton, true)
	hbox.Append(setButton, true)
	page1.Append(hbox, false)

	hbox = newHorizontalBox()
	getButton = ui.NewButton("Get Group Text")
	getButton.OnClicked(func(*ui.Button) {
		entry.SetText(page2group.Title())
	})
	setButton = ui.NewButton("Set Group Text")
	setButton.OnClicked(func(*ui.Button) {
		page2group.SetTitle(entry.Text())
	})

	// compose
	hbox.Append(getButton, true)
	hbox.Append(setButton, true)
	page1.Append(hbox, false)

	// hbox
	hbox = newHorizontalBox()
	hbox.Append(spaced, true)
	getButton = ui.NewButton("On")
	getButton.OnClicked(func(*ui.Button) {
		spaced.SetChecked(true)
	})
	hbox.Append(getButton, false)
	getButton = ui.NewButton("Off")
	getButton.OnClicked(func(*ui.Button) {
		spaced.SetChecked(false)
	})
	hbox.Append(getButton, false)
	getButton = ui.NewButton("Show")
	getButton.OnClicked(func(*ui.Button) {
		// TODO
	})
	hbox.Append(getButton, false)
	page1.Append(hbox, false)

	// testBox
	testBox := newHorizontalBox()
	ybutton := ui.NewButton("Button")
	testBox.Append(ybutton, true)
	getButton = ui.NewButton("Show")
	getButton.OnClicked(func(*ui.Button) {
		ybutton.Show()
	})
	testBox.Append(getButton, false)
	getButton = ui.NewButton("Hide")
	getButton.OnClicked(func(*ui.Button) {
		ybutton.Hide()
	})
	testBox.Append(getButton, false)
	getButton = ui.NewButton("Enable")
	getButton.OnClicked(func(*ui.Button) {
		ybutton.Enable()
	})
	testBox.Append(getButton, false)
	getButton = ui.NewButton("Disable")
	getButton.OnClicked(func(*ui.Button) {
		ybutton.Disable()
	})
	testBox.Append(getButton, false)
	page1.Append(testBox, false)

	// hbox
	hbox = newHorizontalBox()
	getButton = ui.NewButton("Show")
	getButton.OnClicked(func(*ui.Button) {
		testBox.Show()
	})
	hbox.Append(getButton, false)
	getButton = ui.NewButton("Hide")
	getButton.OnClicked(func(*ui.Button) {
		testBox.Hide()
	})
	hbox.Append(getButton, false)
	getButton = ui.NewButton("Enable")
	getButton.OnClicked(func(*ui.Button) {
		testBox.Enable()
	})
	hbox.Append(getButton, false)
	getButton = ui.NewButton("Disable")
	getButton.OnClicked(func(*ui.Button) {
		testBox.Disable()
	})
	hbox.Append(getButton, false)

	// composite
	page1.Append(hbox, false)

	page1.Append(label, false)
}
