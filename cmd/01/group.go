// 12 december 2015

package main

import (
	"github.com/andlabs/ui"
)

var page2group *ui.Group

var (
	movingLabel   *ui.Label
	movingBoxes   [2]*ui.Box
	movingCurrent int
)

func moveLabel(*ui.Button) {
	from := movingCurrent
	to := 0
	if from == 0 {
		to = 1
	}
	movingBoxes[from].Delete(0)
	movingBoxes[to].Append(movingLabel, false)
	movingCurrent = to
}

var moveBack bool

const (
	moveOutText  = "Move Page 1 Out"
	moveBackText = "Move Page 1 Back"
)

func movePage1(b *ui.Button) {
	if moveBack {
		mainbox.Delete(1)
		mainTab.InsertAt("Page 1", 0, page1)
		b.SetText(moveOutText)
		moveBack = false
		return
	}
	mainTab.Delete(0)
	mainbox.Append(page1, true)
	b.SetText(moveBackText)
	moveBack = true
}

func makePage2() *ui.Box {
	page2 := newVerticalBox()

	group := newGroup("Moving Label")
	page2group = group
	page2.Append(group, false)
	vbox := newVerticalBox()
	group.SetChild(vbox)

	hbox := newHorizontalBox()
	button := ui.NewButton("Move the Label!")
	button.OnClicked(moveLabel)
	hbox.Append(button, true)
	hbox.Append(ui.NewLabel(""), true)
	vbox.Append(hbox, false)

	hbox = newHorizontalBox()
	movingBoxes[0] = newVerticalBox()
	hbox.Append(movingBoxes[0], true)
	movingBoxes[1] = newVerticalBox()
	hbox.Append(movingBoxes[1], true)
	vbox.Append(hbox, false)

	movingCurrent = 0
	movingLabel = ui.NewLabel("This label moves!")
	movingBoxes[movingCurrent].Append(movingLabel, false)

	hbox = newHorizontalBox()
	button = ui.NewButton(moveOutText)
	button.OnClicked(movePage1)
	hbox.Append(button, false)
	page2.Append(hbox, false)
	moveBack = false

	hbox = newHorizontalBox()
	hbox.Append(ui.NewLabel("Label Alignment Test"), false)
	button = ui.NewButton("Open Menued Window")
	button.OnClicked(func(*ui.Button) {
		w := ui.NewWindow("Another Window", 100, 100, true)
		b := ui.NewVerticalBox()
		b.Append(ui.NewEntry(), false)
		b.Append(ui.NewButton("Button"), false)
		b.SetPadded(true)
		w.SetChild(b)
		w.SetMargined(true)
		w.Show()
	})
	hbox.Append(button, false)
	button = ui.NewButton("Open Menuless Window")
	button.OnClicked(func(*ui.Button) {
		w := ui.NewWindow("Another Window", 100, 100, true)
		//TODO		w.SetChild(makePage6())
		w.SetMargined(true)
		w.Show()
	})
	hbox.Append(button, false)
	button = ui.NewButton("Disabled Menued")
	button.OnClicked(func(*ui.Button) {
		w := ui.NewWindow("Another Window", 100, 100, true)
		w.Disable()
		w.Show()
	})
	hbox.Append(button, false)
	button = ui.NewButton("Disabled Menuless")
	button.OnClicked(func(*ui.Button) {
		w := ui.NewWindow("Another Window", 100, 100, false)
		w.Disable()
		w.Show()
	})
	hbox.Append(button, false)
	page2.Append(hbox, false)

	nestedBox := newHorizontalBox()
	innerhbox := newHorizontalBox()
	innerhbox.Append(ui.NewButton("These"), false)
	button = ui.NewButton("buttons")
	button.Disable()
	innerhbox.Append(button, false)
	nestedBox.Append(innerhbox, false)
	innerhbox = newHorizontalBox()
	innerhbox.Append(ui.NewButton("are"), false)
	innerhbox2 := newHorizontalBox()
	button = ui.NewButton("in")
	button.Disable()
	innerhbox2.Append(button, false)
	innerhbox.Append(innerhbox2, false)
	nestedBox.Append(innerhbox, false)
	innerhbox = newHorizontalBox()
	innerhbox2 = newHorizontalBox()
	innerhbox2.Append(ui.NewButton("nested"), false)
	innerhbox3 := newHorizontalBox()
	button = ui.NewButton("boxes")
	button.Disable()
	innerhbox3.Append(button, false)
	innerhbox2.Append(innerhbox3, false)
	innerhbox.Append(innerhbox2, false)
	nestedBox.Append(innerhbox, false)
	page2.Append(nestedBox, false)

	hbox = newHorizontalBox()
	button = ui.NewButton("Enable Nested Box")
	button.OnClicked(func(*ui.Button) {
		nestedBox.Enable()
	})
	hbox.Append(button, false)
	button = ui.NewButton("Disable Nested Box")
	button.OnClicked(func(*ui.Button) {
		nestedBox.Disable()
	})
	hbox.Append(button, false)
	page2.Append(hbox, false)

	disabledTab := newTab()
	disabledTab.Append("Disabled", ui.NewButton("Button"))
	disabledTab.Append("Tab", ui.NewLabel("Label"))
	disabledTab.Disable()
	page2.Append(disabledTab, true)

	entry := ui.NewEntry()
	readonly := ui.NewEntry()
	entry.OnChanged(func(*ui.Entry) {
		readonly.SetText(entry.Text())
	})
	readonly.SetText("If you can see this, uiEntryReadOnly() isn't working properly.")
	readonly.SetReadOnly(true)
	if readonly.ReadOnly() {
		readonly.SetText("")
	}
	page2.Append(entry, false)
	page2.Append(readonly, false)

	// hbox
	hbox = newHorizontalBox()
	button = ui.NewButton("Show Button 2")
	button2 := ui.NewButton("Button 2")
	button.OnClicked(func(*ui.Button) {
		button2.Show()
	})
	button2.Hide()

	// compose
	hbox.Append(button, true)
	hbox.Append(button2, false)
	page2.Append(hbox, false)

	return page2
}
