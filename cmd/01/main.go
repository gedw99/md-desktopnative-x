// 11 december 2015

package main

import (
	"flag"
	"fmt"
	"testing"

	"github.com/andlabs/ui"
)

var (
	nomenus     = flag.Bool("nomenus", false, "No menus")
	startspaced = flag.Bool("startspaced", false, "Start with spacing")
	swaphv      = flag.Bool("swaphv", false, "Swap horizontal and vertical boxes")
)

var mainbox *ui.Box
var mainTab *ui.Tab

// TestIt ...
func TestIt(t *testing.T) {
	err := ui.Main(xmain)
	if err != nil {
		t.Fatal(err)
	}
}

func main() {
	err := ui.Main(xmain)
	if err != nil {
		fmt.Println(err)
	}
}

func xmain() {
	if !*nomenus {
		// TODO
	}

	w := ui.NewWindow("Main Window", 640, 480, false)
	w.OnClosing(func(*ui.Window) bool {
		ui.Quit()
		return true
	})

	ui.OnShouldQuit(func() bool {
		// TODO
		return true
	})

	// Layout
	mainbox = newHorizontalBox()
	w.SetChild(mainbox)

	outerTab := newTab()
	mainbox.Append(outerTab, true)

	mainTab = newTab()
	outerTab.Append("Original", mainTab)

	// Load up components
	makePage1(w)
	mainTab.Append("Page 1", page1)

	mainTab.Append("Page 2", makePage2())

	// TODO

	if *startspaced {
		setSpaced(true)
	}

	w.Show()
}

var (
	spwindows []*ui.Window
	sptabs    []*ui.Tab
	spgroups  []*ui.Group
	spboxes   []*ui.Box
)

func newWindow(title string, width int, height int, hasMenubar bool) *ui.Window {
	w := ui.NewWindow(title, width, height, hasMenubar)
	spwindows = append(spwindows, w)
	return w
}

func newTab() *ui.Tab {
	t := ui.NewTab()
	sptabs = append(sptabs, t)
	return t
}

func newGroup(title string) *ui.Group {
	g := ui.NewGroup(title)
	spgroups = append(spgroups, g)
	return g
}

func newHorizontalBox() *ui.Box {
	var b *ui.Box

	if *swaphv {
		b = ui.NewVerticalBox()
	} else {
		b = ui.NewHorizontalBox()
	}
	spboxes = append(spboxes, b)
	return b
}

func newVerticalBox() *ui.Box {
	var b *ui.Box

	if *swaphv {
		b = ui.NewHorizontalBox()
	} else {
		b = ui.NewVerticalBox()
	}
	spboxes = append(spboxes, b)
	return b
}

func setSpaced(spaced bool) {
	for _, w := range spwindows {
		w.SetMargined(spaced)
	}
	for _, t := range sptabs {
		for i := 0; i < t.NumPages(); i++ {
			t.SetMargined(i, spaced)
		}
	}
	for _, g := range spgroups {
		g.SetMargined(spaced)
	}
	for _, b := range spboxes {
		b.SetPadded(spaced)
	}
}
