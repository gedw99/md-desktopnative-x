package main

import (
	"github.com/andlabs/ui"
)

func main() {
	err := ui.Main(func() {

		// widgets
		name := ui.NewEntry()
		button := ui.NewButton("Greet")
		greeting := ui.NewLabel("")

		// layout
		vbox := ui.NewVerticalBox()
		vbox.Append(ui.NewLabel("Enter your name:"), false)
		vbox.Append(name, false)
		vbox.Append(button, false)
		vbox.Append(greeting, false)

		// window
		window := ui.NewWindow("Hello", 640, 480, false)
		window.SetChild(vbox)

		// events
		button.OnClicked(func(*ui.Button) {
			greeting.SetText("Hello, " + name.Text() + "!")
		})
		window.OnClosing(func(*ui.Window) bool {
			ui.Quit()
			return true
		})

		// show
		window.Show()
	})
	if err != nil {
		panic(err)
	}
}
